//-----------------------------------------------
// version 0.1
// (c) Julian Pfeifle 2023, GPL v3
// julian.pfeifle@upc.edu
//-----------------------------------------------

int sizeX = 640;    // window width
int sizeY = 500;    // window height

// the minimum and maximum math coordinates
float currentXMin = -1.5;
float currentXMax =  0.7;
float currentYMin = -1.3;
// currentYMax depends on the aspect ratio of the window and is defined below

int nPixels = 500; // each axis gets subdivided into this many pixels
int maxIter = 500; // how often to iterate before deciding that the orbit stays bounded
float maxAbs = 2.0; // the distance from the origin when we decide the orbit is unbounded

// the color gradient for the background, those points whose orbit escapes to infinity
color minEscapeColor = color(0,0,255);
color maxEscapeColor = color(255,0,0);
float colorExponent = .1; // controls how sharply the color gradient drops off

// The color for the body of the Mandelbrot set
color FractalBackgroundColor = color(0,0,0);

// The color gradient for the orbit originating from the pointer
color minPointerOrbitColor = color(255,255,0);
color maxPointerOrbitColor = color(252,255,209);

//-----------------------------------------------
// end of customization section, now code starts
//-----------------------------------------------

// the aspect ratio of the window
float xyRatio = float(sizeY)/float(sizeX);
float currentYMax =  currentYMin + xyRatio * (currentXMax - currentXMin);

// the size of the current window, in math coordinates
float rangeX = currentXMax - currentXMin;
float rangeY = currentYMax - currentYMin;

// how big a pixel is
float pixelW = sizeX / nPixels;
float pixelH = sizeY / nPixels;

PGraphics bg; // the background for the fractal
PGraphics fg; // the foreground for the selection window

String errorMessage = "";

// the scrollMode button
int buttonX = 60;
int buttonY = 20;
int scrollMode = 0; // 0...zoom in, 1...pan, 2...zoom out

// variables for image handling
float mouseDownX;
float mouseDownY;
boolean drawSelectBox = false;
boolean drawPanArrow = false;

// functions for coordinate changes and drawing on the screen are in the tab coordinateChanges
// functions for zooming and panning are in the tab scrollModes

//--------------------------------------------------------------
// The "standard" function for the Mandelbrot set: z -> z^2 + c
//--------------------------------------------------------------
// If z = x + iy, then 
//   z^2 + c   =   x^2 - y^2 + 2*x*y i +   Re c + i Im c,
// so the real part of the image is 
//   x^2 - y^2 + Re c
// and the imaginary part is 
//   2*x*y + Im c
float reSquare(float rez, float imz, float cx) {
  return rez * rez - imz * imz + cx;
}

float imSquare(float rez, float imz, float cy) {
  return 2 * rez * imz + cy;
}

// what does this function do?
int nItersToEscape(float cx, float cy, boolean drawOrbit, PGraphics pg) {
  float rez = 0;
  float imz = 0;
  float fMaxIter = float(maxIter); // do this outside the loop for efficiency
  float sX0 = cx, sY0 = cy; 
  
  for (int n=0; n<maxIter; n++) {
    float rezTmp = reSquare(rez, imz, cx);
    float imzTmp = imSquare(rez, imz, cy);
    rez = rezTmp;
    imz = imzTmp;
    if (rez * rez + imz * imz > maxAbs) {
      pg.text(n, sX0+8, sY0+4);
      return n;
    }
    if (drawOrbit) {
      color currentColor = lerpColor(minPointerOrbitColor, maxPointerOrbitColor, float(n)/fMaxIter);
      pg.stroke(currentColor);
      pg.fill(currentColor);
      float sX = mathToScreenX(rez);
      float sY = mathToScreenY(imz);
      pg.circle(sX, sY, 10);
      if (0 == n) {
          pg.text(rez, sX-40, sY); 
          pg.text(imz, sX-40, sY+10); 
          sX0 = sX; sY0 = sY;
      }
    }
  }
  return -1;
}

void fractal() {
  bg.beginDraw();
  bg.clear();
  float colorNormalizer = pow(maxIter, colorExponent); // precalculate this here for more efficiency
  color pixelColor = FractalBackgroundColor; // Allocate memory here for more efficiency

  for (float cx = currentXMin; cx < currentXMax; cx+=rangeX/nPixels) {
    for (float cy = currentYMin; cy < currentYMax; cy+=rangeY/nPixels) {
      pixelColor = FractalBackgroundColor;
      int n = nItersToEscape(cx, cy, false, bg);
       if (n != -1) {
         pixelColor = lerpColor(minEscapeColor, maxEscapeColor, pow(n+1, colorExponent)/colorNormalizer); // with n+1 it looks nicer
       }
       pixelOnScreen(bg, cx, cy, pixelColor);
     }
   }
   
   bg.stroke(255);
   bg.fill(255);
   bg.text(nf(rangeX, 2, 10), 30, 10);
   bg.text(nf(rangeY, 2, 10), 110, 10);
   
   if (errorMessage != "") {
      bg.fill(color(0,255,0));
      bg.text(errorMessage, 10, 20);
   }
   
   bg.endDraw();
}

//------------------------------------------------------------
// Setup of window and contents
//------------------------------------------------------------

void settings() {
   size(sizeX, sizeY);   
}

void setup() {
   bg = createGraphics(width, height);
   fg = createGraphics(width, height);

   fractal();
}


void drawOrbit(PGraphics pg, float cx, float cy) {
  int n = nItersToEscape(cx, cy, true, fg); // this time, draw the orbit 
  pg.stroke(255);
  pg.fill(255);
  pg.text(n, 10, 10);
}

// scrollMode has three possible values: zoom in (0), pan (1), zoom out (2)

void drawScrollModeButton() {
  fg.stroke(200);
  fg.fill(color(#0EC121));
  fg.rect(width-buttonX, 0, buttonX, buttonY, 5);
  fg.fill(255);
  if (0 == scrollMode) {
    fg.text("Zoom in", width - buttonX + 10, 15); // this offset kludge is not going to really work if the window size changes
  } else if (1 == scrollMode) {
    fg.text("Pan", width - buttonX + 20, 15); 
  } else {
    fg.text("Zoom out", width - buttonX + 5, 15);
  }
}


//------------------------------------------------------------
// this function gets called every time the cursor moves
//------------------------------------------------------------

void draw() {
  image(bg,0,0);
  
  fg.beginDraw();
  fg.clear();
  
  drawScrollModeButton();
  drawOrbit(fg, screenToMathX(mouseX), screenToMathY(mouseY)); 
  
  if (drawSelectBox) {
    float rectX = mouseX;
    if (rectX > mouseDownX) { 
      float rectXSize = rectX - mouseDownX;
      float rectYSize = xyRatio * rectXSize;
      fg.rect(mouseDownX, mouseDownY, rectXSize, rectYSize);
    }
  }
  
  if (drawPanArrow) {
    fg.stroke(255);
    fg.fill(255);
    fg.line(mouseDownX, mouseDownY, mouseX, mouseY);
  }
  
  fg.endDraw();
  
  image(fg,0,0);
}


void mousePressed() {
   mouseDownX = mouseX;
   mouseDownY = mouseY;
   if (mouseDownX >= width-buttonX && mouseDownY <= buttonY) {
       scrollMode = scrollMode + 1;
       if (3 == scrollMode) { // wrap around to cycle within {0,1,2}
         scrollMode = 0;
       }
   } else if (0 == scrollMode || 2 == scrollMode) {
      drawSelectBox = true;
   } else if (1 == scrollMode) {
      drawPanArrow = true; 
   }
}

// the functions zoomIn, pan and zoomOut are defined in the tab scrollModes
 //<>// //<>//
void mouseReleased() {
  boolean redraw = false;
  if (0 == scrollMode && drawSelectBox) {
    drawSelectBox = false;
    redraw = zoomIn();
  } else if (1 == scrollMode) {
    drawPanArrow = false;
    redraw = pan();
  } else if (2 == scrollMode && drawSelectBox) {
    drawSelectBox = false;
    redraw = zoomOut();
  }
  if (redraw) {
    fractal();
  }
}
