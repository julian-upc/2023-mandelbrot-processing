//-----------------------------------------------
// version 0.1
// (c) Julian Pfeifle 2023, GPL v3
// julian.pfeifle@upc.edu
//-----------------------------------------------

//--------------------------------------------------------------------------
// utility functions for converting math coordinates to screen and viceversa
//--------------------------------------------------------------------------

// We first explain how the formulas in those functions were found.
//
// Don't worry if you don't understand these comments right away. 
// They use computations in homogeneous coordinates, which we will talk about soon.
// If you want to, you can just skip this at first reading, but I'm leaving it here
// in case you want to come back to it once we've covered homogeneous coordinates.
//
// For converting math to screen coordinates, we seek the matrix A such that
//
//      [ rangeX 0 currentXmin ]     [ sizeX 0  0 ]
//   A  [ 0 rangeY currentYMin ]  =  [ 0 -sizeY 0 ]
//      [ 0      0     1       ]     [ 0     0  1 ]
//
// Using computer algebra software (https://sagemath.org), this matrix turns out to be
//
//       [ sizeX/rangeX 0 -sizeX*currentXmin/rangeX ] 
//   A = [ 0 sizeY/rangeY -sizeY*currentYmin/rangeY ]
//       [ 0      0                   1             ]
//
// For the reverse direction, we have 
//           
//            [ rangeX/sizeX 0 currentXmin ]
//   A^{-1} = [ 0 rangeY/sizeY currentYmin ]
//            [ 0            0      1      ]

// For the next function, we need the x-coordinate of  A [ x y 1 ]^T.
// Remember that the current sizeX is stored in the magic variable width,
// and also check that 
// mathToScreenX(screenToMathX(x)) == x and 
// screenToMathX(mathToScreenX(x)) == x
float mathToScreenX(float x) {
   return (x - currentXMin) * width / rangeX;
}

float mathToScreenY(float y) {
   return (y - currentYMin) * height / rangeY; 
}

float screenToMathX(float x) {
   return x * rangeX / width + currentXMin; 
}

float screenToMathY(float y) {
   return y * rangeY / height + currentYMin; 
}

// first parameter controls whether to draw the line on fg or bg
void lineOnScreen(PGraphics pg, float x0, float y0, float x1, float y1, color c) {
  pg.stroke(c);
  pg.line(mathToScreenX(x0), mathToScreenY(y0), mathToScreenX(x1), mathToScreenY(y1));   
}

void pixelOnScreen(PGraphics pg, float x0, float y0, color c) {
  pg.fill(c);
  pg.stroke(c);
  pg.rect(mathToScreenX(x0), mathToScreenY(y0), pixelW, pixelH);   
}

void circleOnScreen(PGraphics pg, float x0, float y0, color c) {
  pg.stroke(c);
  pg.circle(mathToScreenX(x0), mathToScreenY(y0), 10);   
}

void textOnScreen(PGraphics pg, float c, float x0, float y0) {
  pg.text(c, mathToScreenX(x0), mathToScreenY(y0));   
}

void textOnScreen(PGraphics pg, String str, float x0, float y0) {
  pg.text(str, mathToScreenX(x0), mathToScreenY(y0));   
}
