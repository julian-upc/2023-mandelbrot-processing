//-----------------------------------------------
// version 0.1
// (c) Julian Pfeifle 2023, GPL v3
// julian.pfeifle@upc.edu
//-----------------------------------------------

//---------------------------------------------------------------
// This file implements the three behaviors: zoom in, pan, zoom out
//
//  YOU CAN SKIP THIS ON YOUR FIRST READING!
//
//---------------------------------------------------------------

boolean zoomIn() {
   // save the current values in case accuracy gets too low
   float oldXMin = currentXMin;
   float oldYMin = currentYMin;
   float oldXMax = currentXMax;
   float oldYMax = currentYMax;
   
   // now adjust parameters so that only the part of the image inside the selected box gets drawn 
   currentXMin = screenToMathX(mouseDownX);
   currentYMin = screenToMathY(mouseDownY);
   
   float rectXSize = mouseX - mouseDownX;
   
   currentXMax = screenToMathX(mouseDownX + rectXSize);
   currentYMax = currentYMin + xyRatio * (currentXMax - currentXMin);
   
   rangeX = currentXMax - currentXMin;
   rangeY = currentYMax - currentYMin;
   
   if (rangeX < 1e-5 || rangeY < 1e-5) {
      errorMessage = "Accuracy limit reached";
      currentXMin = oldXMin;
      currentXMax = oldXMax;
      currentYMin = oldYMin;
      currentYMax = oldYMax;
      rangeX = currentXMax - currentXMin;
      rangeY = currentYMax - currentYMin;
      return false; 
   }
   return true;
}

boolean pan() {
  float deltaXScr = mouseX - mouseDownX;
  float deltaYScr = mouseY - mouseDownY;
  if (abs(deltaXScr) < 2 || abs(deltaYScr) < 2) {
    // this happens when clicking the button and changing to "Pan" mode,
    // since the use is not actually panning, we return false to not redraw the fractal
    return false;
  }
  
  float deltaX = deltaXScr * rangeX/float(width);
  float deltaY = deltaYScr * rangeY/float(height);
  currentXMin -= deltaX;
  currentXMax -= deltaX;
  currentYMin -= deltaY;
  currentYMax -= deltaY;
  return true;
}

boolean zoomOut() {
  // convert the mouseDown event to screen coordinates
  mouseDownX = screenToMathX(mouseDownX);
  mouseDownY = screenToMathY(mouseDownY);
  
  // take the x value for the mouseUp event from the pointer position
  float mouseUpX = screenToMathX(mouseX);
  
  // but adjust the y value using the window ratio to prevent distortion 
  float mouseUpY = mouseDownY + xyRatio * (mouseUpX - mouseDownX);
  
  // To zoom the current window out, we need to implement this transformation:
  //
  //  (mouseDownX, mouseDownY)                    (currentXMin, currentYMin)
  //             +----------------------+                 +----------------------+
  //             |                      |       A         |                      |
  //             |                      |      -->        |                      |
  //             +----------------------+                 +----------------------+
  //                           (mouseUpX, mouseUpY)                  (currentXMax, currentYMax)
  //
  // So we are looking for the affine transform A that sends the columns on the left to the columns on the right:
  //
  //    [ mouseDownX  mouseDownX mouseUpX   ]     [ currentXMin  currentXMin currentXMax ]
  // A  [ mouseDownY  mouseUpY   mouseDownY ]  =  [ currentYMin  currentYMax currentYMin ]
  //    [     1           1          1      ]     [     1             1          1       ]
  //
  // An algebra system (https://sagemath.org) tells us that (with the obvious abbreviations)
  //
  //     [       -(cxmax - cxmin)/(mdx - mux)                                   0 (cxmax*mdx - cxmin*mux)/(mdx - mux)]
  // A = [                                  0        -(cymax - cymin)/(mdy - muy) (cymax*mdy - cymin*muy)/(mdy - muy)]
  //     [                                  0                                   0                                   1]
  //
  // This transform takes the selection window to the current view window.
  // In order to actually zoom out, we need to apply the transform to the *view window*, not the selection window. 
  //
  // So we first apply this matrix A to the upper left hand corner point of the view window, using our algebra system:
  // 
  // A [cxmin, cymin, 1] = (-(cxmax*cxmin - cxmin^2 - cxmax*mdx + cxmin*mux)/(mdx - mux), -(cymax*cymin - cymin^2 - cymax*mdy + cymin*muy)/(mdy - muy), 1)
  //
  // and finally, calculate the image of the diagonal vector of the view window to get to the opposite corner point of the image:
  // 
  // A [cxmax-cxmin, cymax-cymin, 0]) = (-(cxmax - cxmin)^2/(mdx - mux), -(cymax - cymin)^2/(mdy - muy), 0)

  float dx = mouseUpX - mouseDownX; // notice the flipped sign. This dx is positive
  float dy = mouseUpY - mouseDownY;
  
  // now the first formula for A [cxmin, cymin, 1] 
  float newcxmin = (currentXMax * currentXMin - currentXMin * currentXMin - currentXMax * mouseDownX + currentXMin * mouseUpX) / dx;  
  float newcymin = (currentYMax * currentYMin - currentYMin * currentYMin - currentYMax * mouseDownY + currentYMin * mouseUpY) / dy;  
  
  // and the second formula for A [cxmax-cxmin, cymax-cymin, 0])
  rangeX = sq(rangeX) / dx;
  rangeY = sq(rangeY) / dy;
  
  // now we can calculate the new coordinates of the corners of the window:
  currentXMin = newcxmin;
  currentYMin = newcymin;
  currentXMax = currentXMin + rangeX;
  currentYMax = currentYMin + rangeY;
  return true;
}
